import React from "react";
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import './App.css';
import AnimeQuotes from "./containers/AnimeQuotes/AnimeQuotes";
import AboutUs from "./components/About/AboutUs";
import AnimeList from "./components/AnimeList/AnimeList";
import FindAnime from "./components/FindByAnime/FindAnime";

function App() {
  return (
      <div className="app">
          <BrowserRouter>
              <Switch>
                  <Route path="/" exact component={AnimeQuotes}/>
                  <Route path="/about-us" component={AboutUs}/>
                  <Route path="/anime-list" component={AnimeList}/>
                  <Route path="/find-by-anime" component={FindAnime}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
}

export default App;
