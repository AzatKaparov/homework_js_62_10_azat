import React, {useState, useEffect} from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import Quote from "../../components/AnimeQuotes/Quote";
import Navbar from "../../components/Navbar/Navbar";
import Button from '@material-ui/core/Button';


const AnimeQuotes = () => {
    const [quotes, setQuotes] = useState([]);
    const [nextQuotes, setNextQuotes] = useState(0);

    useEffect(() => {
        const fetchData = async () => {
            const quotesResponse = await axios.get("https://animechan.vercel.app/api/quotes");
            setQuotes(quotesResponse.data);
        };
        fetchData().catch(console.error);
    }, [nextQuotes]);

    return (
        <div className="main-content">
            <Navbar/>
            <div className="container" style={{padding: 10}}>
                <Button
                    onClick={() => setNextQuotes(nextQuotes + 1)}
                    variant="contained"
                    color="primary">
                    Get new quotes
                </Button>
                {quotes.map(quote => {
                    return (
                        <Quote
                            key={uuidv4()}
                            anime={quote.anime}
                            character={quote.character}
                            text={quote.quote}
                        />
                    );
                })}
            </div>
        </div>
    );
};

export default AnimeQuotes;