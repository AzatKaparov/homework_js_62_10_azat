import React from 'react';
import {AppBar, IconButton, Toolbar, Typography} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import './Navbar.css';

const Navbar = props => {

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="menu">
                </IconButton>
                <Typography variant="h6">
                    Anime quotes
                </Typography>
                <div className="nav-links">
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/about-us">About us</NavLink>
                    <NavLink to="/anime-list">Anime list</NavLink>
                    <NavLink to="/find-by-anime">Find by anime</NavLink>
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;