import React from 'react';
import Navbar from "../Navbar/Navbar";
import "./AboutUs.css";

const AboutUs = () => {
    return (
        <div className="about-us">
            <Navbar/>
            <div className="container">
                <div className="about-block">
                    <img src="https://github.com/rocktimsaikia/anime-chan/raw/main/images/animechan_logo.png" alt="Animechan's avatar"/>
                    <h1>Animechan</h1>
                    <p>A free restful API serving quality anime quotes</p>
                    <a href="https://animechan.vercel.app/">Explore the api</a>
                </div>
            </div>
        </div>
    );
};

export default AboutUs;