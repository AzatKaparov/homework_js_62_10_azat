import React, {useState, useEffect} from 'react';
import Navbar from "../Navbar/Navbar";
import axios from "axios";
import { v4 as uuidv4 } from 'uuid';
import TextField from "@material-ui/core/TextField";
import './AnimeList.css';

const AnimeList = () => {
    const [animeList, setAnimeList] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [inpVal, setInpVal] = useState('');

    const handleInputChange = e => {
        setInpVal(e.target.value);
        const filtered = animeList.filter(el => {
            return el.toLowerCase().includes(inpVal.toLowerCase())
        })
        setFilteredData(filtered);
    };

    useEffect(() => {
        const fetchData = async () => {
            const animeResponse = await axios.get("https://animechan.vercel.app/api/available/anime");
            setAnimeList(animeResponse.data);
        };
        fetchData().catch(console.error);
    }, [])

    return (
        <div className="anime-list">
            <Navbar/>
            <div className="container">
                <TextField style={{marginTop: 20}} onChange={handleInputChange} id="outlined-basic" label="Enter anime name" variant="outlined" />
                <div className="anime-blocks">
                    <ul className="find-anime-ul">
                        <h5>Find available anime by name</h5>
                        {filteredData.length > 0
                            ? filteredData.map(item => (
                                <li className="anime-li" key={uuidv4()}>{item}</li>
                            ))
                            : "Haven't find anything (or you didn't enter anything)"
                        }
                    </ul>
                    <ul className="all-anime-ul">
                        {animeList.map(item => (
                            <li>{item}</li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default AnimeList;