import React, {useState, useEffect} from 'react';
import axios from "axios";
import {v4 as uuidv4} from 'uuid';
import './FindByAnime.css';
import Navbar from "../Navbar/Navbar";
import TextField from "@material-ui/core/TextField";
import Quote from "../AnimeQuotes/Quote";
import Button from "@material-ui/core/Button";

const FindAnime = () => {
    const [inpVal, setInpVal] = useState('');
    const [selectedAnime, setSelectedAnime] = useState(null);
    const [page, setPage] = useState(1);


    const handleSubmit = e => {
        e.preventDefault();
        const inp = document.getElementById("findByNameInp");
        setInpVal(inp.value);
    }

    const handleNextPage = () => {
        setPage(page + 1);
    }

    const handlePreviousPage = () => {
        if (page > 1) {
            setPage(page - 1);
        }
    }


    useEffect(() => {
        const fetchData = async () => {
            if (inpVal !== '') {
                const animeResponse = await axios.get(`https://animechan.vercel.app/api/quotes/anime?title=${inpVal}&page=${page}`);
                setSelectedAnime(animeResponse.data);
            }
        };
        fetchData().catch(console.error);
    }, [inpVal, page])


    return (
        <>
            <Navbar/>
            <div className="container">
                <form action="" className="find-anime-form" onSubmit={handleSubmit}>
                    <TextField id="findByNameInp" label="Find quotes by anime" />
                    <Button type="submit" variant="contained" color="primary">
                        Find
                    </Button>
                </form>
                {selectedAnime &&
                    <>
                        <Button
                            onClick={handlePreviousPage}
                            type="button"
                            variant="contained"
                            color="primary">
                            Previous
                        </Button>
                        <Button
                            onClick={handleNextPage}
                            type="button"
                            variant="contained"
                            color="primary">
                            Next
                        </Button>
                    </>
                }
                <div className="quotes">
                    {selectedAnime &&
                        selectedAnime.map(quote => {
                        return (
                            <Quote
                                key={uuidv4()}
                                anime={quote.anime}
                                character={quote.character}
                                text={quote.quote}
                            />
                        );
                    })}
                </div>
            </div>
        </>
    );
};

export default FindAnime;