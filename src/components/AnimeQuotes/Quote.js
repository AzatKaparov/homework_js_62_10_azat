import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

const Quote = ({ anime, character, text }) => {
    const classes = useStyles();

    return (
        <div>
            <Card className={classes.root} style={{margin: 15}}>
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        {anime}
                    </Typography>
                    <Typography className={classes.pos} variant="h5" component="h2">
                        {character}
                    </Typography>
                    <Typography variant="body2" component="p">
                        {text}
                    </Typography>
                </CardContent>
            </Card>
        </div>
    );
};

export default Quote;